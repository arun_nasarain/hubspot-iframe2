//import CallingExtensions, { Constants } from "@hubspot/calling-extensions-sdk";
//const axios = require('axios')
//const CallingExtensions =require('"@hubspot/calling-extensions-sdk"') ;//"../../../src/CallingExtensions";

import axios from "axios";
import CallingExtensions from "../src/CallingExtensions";
import { errorType } from "../src/Constants";
const callback = () => {

  const defaultSize = {
    width: 350,
    height: 500
  };
console.log("hi")
  const state = {};

  const cti = new CallingExtensions({
    debugMode: true,
    eventHandlers: {
      onReady: () => {
        cti.initialized({
          isLoggedIn: true,
          sizeInfo: defaultSize
        });
      },
      onDialNumber: (data, rawEvent) => {
        document.getElementById("notes").value=""
        document.getElementById("calls").style.display = "block";
        document.getElementById("footer").style.display = "none";
        document.getElementById("callStatus").innerHTML = "Calling Customer...";

        let phoneNumber=data.phoneNumber
        state.phoneNumber=data.phoneNumber
        state.hubspotId=data.portalId
        state.ownerId=data.ownerId
        state.timestamp=data.startTimestamp
        state.contactid=data.calleeInfo.calleeId
        state.note

        console.log("ondialnumber");
        //var ele=document.getElementById("endcall");
        console.log(rawEvent);
        //const { phoneNumber } = data;
        document.getElementById("mobile").innerHTML = state.phoneNumber;
        cti.outgoingCall({
          createEngagement: true,
          phoneNumber
        })
        //ele.onclick=endcall;

        //axios.post(`http://localhost:3000/v2/accounts/exotel119/call?number="${state.phoneNumber}"&ownerid="124"`).then((response) => {
        axios.post(`https://hubspot-middleware.exotel.in/v2/accounts/${state.hubspotId}/call?number=${state.phoneNumber}&ownerid=${state.ownerId}`)
        .then(async (response) => {

        console.log("response data - ",response);
        state.callSid=response.data.Call.Sid;
        state.exotelSid=response.data.Call.AccountSid;
        
        let j=0;
        for(var i=0;i<10,j<100;i++){
          //setTimeout(()=>{
            j=await callback(i)
          //},1000) 
        }
        }).catch(( error)=>{
          console.log(error.response.data.description)
          callFailed(error.response.data.description)
        });
      },
      onEngagementCreated: (data, rawEvent) => {
        const { engagementId } = data;
        state.engagementId = engagementId;
      },
      onVisibilityChanged: (data, rawEvent) => {
        console.log(data)
      }
    }
  });

function callFailed(error){
    cti.sendError({
    type: errorType.GENERIC,
    message: error
  });
  cti.callEnded({
    engagementId: state.engagementId
  });
  cti.callCompleted({
    engagementId: state.engagementId,
    hideWidget:true
    });
}

async function callback(i){
  return new Promise((resolve, reject) => {
    console.log("iteration-",i)
  axios.get(`https://hubspot-middleware.exotel.in/v2/accounts/${state.hubspotId}/callbackpolling`, {
    params: {
      CallSid: state.callSid,
      accountSid:state.exotelSid,
      timestamp:state.timestamp,
      contactId:state.contactid,
      ownerId:state.ownerId
    }
  })
  .then((response)=>{
    if(response.data.hasValue==true){
      console.log("status callback received",response.data)
      endcall();
      i+=1000
      console.log(i)
      state.engId=response.data.engId
      //return i
    }
    console.log(i)
    console.log("callback resp",response.data)
    console.log("engid",state.engId)
    //return i
    resolve(i)
  }).catch((error)=>{
    console.log("error",error)
  });
  });
  
}

function endcall(){
    cti.callEnded({
      engagementId: state.engagementId
    });

    document.getElementById("callStatus").innerHTML = "Call Ended";
    //document.getElementById("endcall").style.display = "none";

    //document.getElementById("calls").style.display = "none";
    document.getElementById("footer").style.display = "block";
    
    var save=document.getElementById("save");
    save.onclick=completecall;
 }

 function completecall(){

  state.note=document.getElementById("notes").value
  document.getElementById("notes").value=""

  //axios.post(`http://localhost:3000/v2/accounts/${state.hubspotId}/call/createEngagement`,{
  axios.put(`https://hubspot-middleware.exotel.in/v2/accounts/${state.exotelSid}/engagement`,{
  
    id:state.engId,
    timestamp:state.timestamp,
    note:state.note,
  }).then((response) => {
    console.log("response data - ",response);
    }, (error) => {
      console.log(error);
    });

    console.log("state",state);
    cti.callCompleted({
    engagementId: state.engagementId,
    hideWidget:true
    });
    console.log("call completed")
  
}

};

//callback();

if (
  document.readyState === "interactive" ||
  document.readyState === "complete"
) {
  console.log("hi")
  window.setTimeout(() => callback(), 10);
} else {
  document.addEventListener("DOMContentLoaded", callback);
}
