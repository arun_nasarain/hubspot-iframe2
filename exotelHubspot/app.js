const express = require('express')
const path =require('path')
const app = express()
const port = 9025
const fs=require('fs')
const https=require('https')

app.use(express.static('./'))
//app.use(express.static(path.join(__dirname,'/image')))

app.get('/health', (req, res) => {
  res.send('health')
})

app.get('/', (req, res) => {
  res.sendFile('./index.html',{root:__dirname})
})

/*var privateKey = fs.readFileSync( 'localhost-key.pem' );
var certificate = fs.readFileSync( 'localhost.pem' );

https.createServer({
  key: privateKey,
  cert: certificate
}, app).listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
});*/

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
